import {Facts} from '../models';

export interface AppState {
  facts?: Facts;
}
