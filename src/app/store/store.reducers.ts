import { factsReducer } from '../components/facts.reducer';
import { combineReducers } from 'redux';


// Define the global store shape by combining our application's
// reducers together into a given structure.
export const rootReducer = combineReducers({
  facts: factsReducer
});
