import { Routes } from '@angular/router';
import { FactCardListComponent } from './components/fact-card-list/fact-card-list.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: '/facts', pathMatch: 'full' },
  { path: 'facts', component: FactCardListComponent },
];
