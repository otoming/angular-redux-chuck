import {Category} from './category';
import {Fact} from './fact';

export interface Facts {
  fetchStatus: string;
  categories: [Category];
  facts: Fact;
}
