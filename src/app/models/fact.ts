import { Category } from './category';

export interface Fact {
  categories: Category;
  created_at: string;
  icon_url: string;
  id: string;
  updated_at: string;
  url: string;
  value: string;
}
